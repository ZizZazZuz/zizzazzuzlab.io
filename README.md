## MWO Skills Tree
(aka mwoskill)

This is a web implementation of the new Mechwarrior Online Skills Tree, so
people can tinker around without having to be logged into the MWO Client.

https://kitlaan.gitlab.io/mwoskill/
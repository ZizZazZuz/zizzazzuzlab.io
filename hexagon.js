// Hex math defined here:
// * http://blog.ruslans.com/2011/02/hexagonal-grid-math.html
// * http://www.redblobgames.com/grids/hexagons/

function HexagonGrid(canvas, radius, spacing) {
    this.radius = radius;
    this.spacing = spacing;

    this.height = Math.sqrt(3) * (radius);
    this.width = 2 * (radius);

    // horizontal distance to adjacent hexagon
    this.side = (3 / 2) * (radius);

    this.spaceX = spacing * Math.sqrt(3) / 2;
    this.spaceY = spacing;

    this.canvas = canvas;
    this.context = this.canvas.getContext('2d');

    this.canvasOriginX = 0;
    this.canvasOriginY = 0;

    this.strokeWidth = 2;
    this.lineWidth = 4;
};

HexagonGrid.prototype.bindListener = function(action, fn, bindctx) {
    if (!bindctx) {
        bindctx = this;
    }
    this.canvas.addEventListener(action, fn.bind(bindctx), false);
};

HexagonGrid.prototype.setOrigin = function(originX, originY) {
    this.canvasOriginX = originX;
    this.canvasOriginY = originY;
};

HexagonGrid.prototype.getOrigin = function() {
    return { x: this.canvasOriginX, y: this.canvasOriginY };
};

HexagonGrid.prototype.drawHexAtColRow = function(col, row, fillColor, strokeColor, strokeStyle) {
    var origin = this.getOffsetAtColRow(col, row);

    origin.x += this.canvasOriginX;
    origin.y += this.canvasOriginY;

    this.drawHex(origin, fillColor, strokeColor, strokeStyle);
};

HexagonGrid.prototype.drawHexDebugAtColRow = function(col, row) {
    var origin = this.getOffsetAtColRow(col, row);

    origin.x += this.canvasOriginX;
    origin.y += this.canvasOriginY;

    this.drawDebug(origin.x, origin.y, col + "," + row);
};

HexagonGrid.prototype.clear = function() {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
}

HexagonGrid.prototype.drawLine = function(col0, row0, col1, row1, color, dash) {
    var draw0 = this.getCenterAtColRow(col0, row0);
    draw0.x += this.canvasOriginX;
    draw0.y += this.canvasOriginY;

    var draw1 = this.getCenterAtColRow(col1, row1);
    draw1.x += this.canvasOriginX;
    draw1.y += this.canvasOriginY;

    // TODO: edge-to-edge, rather than center-to-center

    this.context.strokeStyle = color;
    this.context.lineWidth = this.lineWidth;

    if (!dash) {
        dash = [];
    }
    this.context.setLineDash(dash);

    this.context.beginPath();
    this.context.moveTo(draw0.x, draw0.y);
    this.context.lineTo(draw1.x, draw1.y);

    this.context.stroke();
};

HexagonGrid.prototype.drawTriangle = function(pts, fillColor, strokeColor) {
    if (strokeColor) {
        this.context.strokeStyle = strokeColor;
    }

    this.context.beginPath();
    this.context.moveTo(this.canvasOriginX + pts[0].x, this.canvasOriginY + pts[0].y);
    this.context.lineTo(this.canvasOriginX + pts[1].x, this.canvasOriginY + pts[1].y);
    this.context.lineTo(this.canvasOriginX + pts[2].x, this.canvasOriginY + pts[2].y);
    this.context.closePath();

    if (fillColor) {
        this.context.fillStyle = fillColor;
        this.context.fill();
    }

    this.context.stroke();
};

HexagonGrid.prototype.drawHex = function(origin, fillColor, strokeColor, dash) {
    var x0 = origin.x;
    var y0 = origin.y;

    if (strokeColor) {
        this.context.strokeStyle = strokeColor;
    }

    if (!dash) {
        dash = [];
    }
    this.context.setLineDash(dash);

    this.context.lineWidth = this.strokeWidth;

    // start at top-left
    this.context.beginPath();
    this.context.moveTo(x0 + (this.width - this.side), y0);
    this.context.lineTo(x0 + this.side, y0);
    this.context.lineTo(x0 + this.width, y0 + (this.height / 2));
    this.context.lineTo(x0 + this.side, y0 + this.height);
    this.context.lineTo(x0 + (this.width - this.side), y0 + this.height);
    this.context.lineTo(x0, y0 + (this.height / 2));
    this.context.closePath();

    if (fillColor) {
        this.context.fillStyle = fillColor;
        this.context.fill();
    }

    this.context.stroke();
};

HexagonGrid.prototype.drawDebug = function(x0, y0, debugText) {
    this.context.font = "10px sans-serif";
    this.context.fillStyle = "#555";
    this.context.textAlign = "left";
    this.context.textBaseline = "middle";
    this.context.fillText(debugText,
                          x0 + (this.width - this.side),
                          y0 + (this.height - 7));
};

HexagonGrid.prototype.getCenterAtColRow = function(col, row) {
    var offset = this.getOffsetAtColRow(col, row);

    offset.x += (this.width / 2);
    offset.y += (this.height / 2);

    return offset;
};

HexagonGrid.prototype.getOffsetAtColRow = function(col, row) {
    var drawX = col * (this.side + this.spaceX);

    var drawY = row * (this.height + this.spaceY);
    if (col % 2 == 0) {
        drawY += (this.height + this.spaceY) / 2;
    }

    return { x: drawX, y: drawY };
};

// Recusivly step up to the body to calculate canvas offset.
HexagonGrid.prototype.getRelativeCanvasOffset = function() {
    var x = 0;
    var y = 0;

    var layoutElement = this.canvas;
    if (layoutElement.offsetParent) {
        do {
            x += layoutElement.offsetLeft;
            y += layoutElement.offsetTop;

            layoutElement = layoutElement.offsetParent;
        } while (layoutElement);

        return { x: x, y: y };
    }
}

HexagonGrid.prototype.getSelectedTile = function(mouseX, mouseY) {
    var offset = this.getRelativeCanvasOffset();
    var cursor = { x: mouseX - offset.x, y: mouseY - offset.y };

    var rest = this.width - this.side;

    var sideX = this.side + this.spaceX;
    var fullY = this.height + this.spaceY;

    // get a rough idea of where we are
    var col = Math.floor(cursor.x / sideX);
    var row = (col % 2 == 0)
            ? Math.floor((cursor.y - (fullY / 2)) / fullY)
            : Math.floor(cursor.y / fullY);

    // inside this hexagon?
    if (this.isPointInHexagon(cursor, this.getOffsetAtColRow(col, row))) {
        return { row: row, col: col };
    }

    // inside the hexagon to the top-left?
    var rowTL = row - ((col % 2 == 0) ? 0 : 1);
    if (this.isPointInHexagon(cursor, this.getOffsetAtColRow(col - 1, rowTL))) {
        return { row: rowTL, col: col - 1 };
    }

    // inside the hexagon to the bottom-left?
    var rowTR = row + ((col % 2 == 0) ? 1 : 0);
    if (this.isPointInHexagon(cursor, this.getOffsetAtColRow(col - 1, rowTR))) {
        return { row: rowTR, col: col - 1 };
    }

    return;
};

HexagonGrid.prototype.sign = function(p1, p2, p3) {
    return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
};

HexagonGrid.prototype.isPointInTriangle = function(pt, tri) {
    var b1, b2, b3;

    b1 = this.sign(pt, tri[0], tri[1]) < 0.0;
    b2 = this.sign(pt, tri[1], tri[2]) < 0.0;
    b3 = this.sign(pt, tri[2], tri[0]) < 0.0;

    return ((b1 == b2) && (b2 == b3));
};

HexagonGrid.prototype.isPointInRectangle = function(pt, sq) {
    return (pt.x > sq[0].x) && (pt.x < sq[1].x) &&
           (pt.y > sq[0].y) && (pt.y < sq[1].y);
}

HexagonGrid.prototype.isPointInHexagon = function(pt, origin) {
    var x0 = origin.x;
    var y0 = origin.y;

    // left side
    var t0 = [
        { x: x0 + (this.width - this.side), y: y0 },
        { x: x0 + (this.width - this.side), y: y0 + this.height },
        { x: x0, y: y0 + (this.height / 2) }
    ];
    if (this.isPointInTriangle(pt, t0)) {
        return true;
    }

    // center
    var t1 = [
        { x: x0 + (this.width - this.side), y: y0 },
        { x: x0 + this.side, y: y0 + this.height }
    ];
    if (this.isPointInRectangle(pt, t1)) {
        return true;
    }

    // right side
    var t2 = [
        { x: x0 + this.side, y: y0 },
        { x: x0 + this.width, y: y0 + (this.height / 2) },
        { x: x0 + this.side, y: y0 + this.height }
    ];
    if (this.isPointInTriangle(pt, t2)) {
        return true;
    }

    return false;
};

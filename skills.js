
function MwoSkills(hexagonGrid, lineGrid) {
    this.version = "0.2-ALPHA";

    this.hex = hexagonGrid;
    this.lines = lineGrid;

    var offset = -100;
    this.hex.setOrigin((this.hex.canvas.width - this.hex.width) / 2 + offset, 0);
    this.lines.setOrigin((this.lines.canvas.width - this.lines.width) / 2 + offset, 0);

    //this.debug = true;

    var dlButton = document.getElementById("downloader");
    dlButton.addEventListener("click", this.downloadEvent.bind(this));

    var selectallButton = document.getElementById("selectallnodes");
    selectallButton.addEventListener("click", this.selectallnodesEvent.bind(this));

    var clearButton = document.getElementById("clearnodes");
    clearButton.addEventListener("click", this.clearnodesEvent.bind(this));

    window.addEventListener("keypress", this.keyEvent.bind(this));

    this.isMobile = (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf("IEMobile") !== -1);

    this.resetSkills();

    this.activeSection = "";
    this.unlockedSkills = [];
};

MwoSkills.prototype.resetSkills = function() {
    this.lines.clear();
    this.hex.clear();

    // list of versions, 'skill-versions.json'
    this.versionFiles = [];

    // list of skill information, 'skill-versions.json'
    this.skillTypes = {};

    // data loaded from the skill itself
    this.skillDate = "";
    this.skillVersion = "";
    this.skillSections = [];
    this.skillData = {};
};

MwoSkills.prototype.initialize = function() {
    this.resetSkills();

    this.loadSessionState();

    this.loadJSON("skill-types.json", function(data) {
        this.skillTypes = data;
    });

    this.loadJSON("skill-versions.json", function(data) {
        this.versionFiles = data;
        this.populateSkillModal();
    });
};

MwoSkills.prototype.loadSkills = function(filename) {
    this.loadJSON(filename, function(data) {
        if (data && "sections" in data) {
            this.skillDate = data.date;
            this.skillVersion = data.version;

            // put the data somewhere convenient
            this.skillSections = [];
            this.skillData = {};
            for (var s of data.sections) {
                this.skillSections.push(s.title);
                this.skillData[s.title] = s;
            }

            // version information
            var geninfo = document.getElementById('GeneralInfo');
            this.clearElement(geninfo);
            geninfo.appendChild(document.createElement('br'));
            geninfo.appendChild(document.createTextNode(this.skillVersion));
            geninfo.appendChild(document.createElement('br'));
            geninfo.appendChild(document.createTextNode("(" + this.skillDate + ")"));

            // left menu for "sections"
            var nav = document.getElementById('NavPane');
            this.clearElement(nav);
            var lastCategory = "";
            for (var s of data.sections) {
                if (lastCategory != s.category) {
                    lastCategory = s.category;

                    var c = document.createElement('span');
                    c.appendChild(document.createTextNode(s.category));
                    nav.appendChild(c);
                }

                var me = this;
                var a = document.createElement('a');
                a.appendChild(document.createTextNode(s.title));
                a.target = s.title;
                a.id = "nav_" + s.title;
                a.onclick = function() {
                    me.setActiveSection(this.target);
                };
                nav.appendChild(a);
            }

            // clean up unlocked skills
            var oldunlocked = this.unlockedSkills;
            this.unlockedSkills = [];
            for (var s in this.skillData) {
                for (var c of this.skillData[s].cells) {
                    var key = s + "," + c.col + "," + c.row;
                    if (oldunlocked.indexOf(key) >= 0) {
                        this.unlockedSkills.push(key);
                    }
                }
            }

            this.updateSkillCount();
            this.computeQuirkRollup();

            // make sure the section that was active, can still be active...
            if (!this.activeSection || !this.skillSections.includes(this.activeSection)) {
                this.setActiveSection(this.skillSections[0]);
            }
            else {
                // force a redraw
                this.setActiveSection(this.activeSection);
            }
        }
    });
};

MwoSkills.prototype.populateSkillModal = function() {
    var options = document.getElementById("SkillVersions").options;
    for (var v of this.versionFiles) {
        var o = document.createElement("option");
        o.text = v.version + " (" + v.date + ")";
        o.value = v.file;
        options.add(o);
    }

    var me = this;
    var button = document.getElementById("SkillVersionsButton");
    button.onclick = function() {
        me.loadSkills(options[options.selectedIndex].value);

        var modal = document.getElementById("SelectVersionModal");
        modal.style.display = "none";
        window.onclick = undefined;
    }
};

MwoSkills.prototype.renderSection = function(section) {
    if (!this.skillData) {
        return false;
    }
    if (!(section in this.skillData)) {
        return false;
    }

    this.lines.clear();
    this.hex.clear();

    this.renderSectionOnHexGrid(section, this.hex, this.lines);
};

MwoSkills.prototype.renderSectionOnHexGrid = function(section, hexGrid, lineGrid) {
    var ctx = lineGrid.context;

    var origin = lineGrid.getOrigin();
    origin.x += this.lines.width / 2;

    // NOTE: HERE BE MAGIC NUMBERS!
    ctx.font = "30px sans-serif";
    ctx.fillStyle = "#aaa";
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";
    ctx.fillText(this.skillData[section].title, origin.x, origin.y + 28);

    function sortCells(a, b) {
        return (a.row * 2 + (a.col % 2 == 0 ? 1 : 0)) -
               (b.row * 2 + (b.col % 2 == 0 ? 1 : 0));
    }

    var invalidCells = this.computeSectionValid(section);

    // draw the cells from top-to-bottom
    var cells = this.skillData[section].cells;
    for (var c of cells.sort(sortCells)) {
        var valid = invalidCells.indexOf(c.col + "," + c.row) < 0;
        this.drawCellOnHexGrid(section, c, hexGrid, lineGrid, valid);
    }

    // draw informational stuffs
    if (invalidCells.length > 0) {
        // TODO: triangle warning, red background?
        ctx.font = "bold 16px sans-serif";
        ctx.fillStyle = "#f33";
        ctx.fillText("INVALID TREE",
                     origin.x + (lineGrid.width * 2),
                     origin.y + 26 + (lineGrid.height / 2));
    }
};

MwoSkills.prototype.drawCellOnHexGrid = function(section, cell, hexGrid, lineGrid, valid) {
    var colors = this.getSkillColors(section, cell.col, cell.row);

    var dash = [];
    if (!valid) {
        colors.stroke = "#f33";
        dash = [7, 3];
    }

    for (var l of cell.links) {
        lineGrid.drawLine(cell.col, cell.row, l.col, l.row, colors.stroke, dash);
    }

    hexGrid.drawHexAtColRow(cell.col, cell.row, colors.fill, colors.stroke, dash);
    if (this.debug) {
        hexGrid.drawHexDebugAtColRow(cell.col, cell.row);
    }

    var origin = hexGrid.getOrigin();

    var center = hexGrid.getCenterAtColRow(cell.col, cell.row);
    center.x += origin.x;
    center.y += origin.y;

    var ctx = hexGrid.context;

    // draw the title

    // NOTE: HERE BE MAGIC NUMBERS!
    ctx.font = "bold 13px sans-serif";
    ctx.fillStyle = colors.text;
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";

    var words = (cell.type).split(" ");

    var title1 = "";
    if (words.length > 1) {
        title1 = words.slice(0, words.length - 1).join(" ");
    }

    var title2 = words[words.length - 1];
    if (cell.level) {
        // NOTE: the "level" just clutters; remove it
        if (this.debug) {
            title2 += " " + cell.level;
        }
    }

    // NOTE: HERE BE MAGIC NUMBERS!
    if (title1) {
        ctx.fillText(title1, center.x, center.y - 12);
        ctx.fillText(title2, center.x, center.y + 2);
    }
    else {
        ctx.fillText(title2, center.x, center.y - 6);
    }

    // draw value

    // NOTE: HERE BE MAGIC NUMBERS!
    ctx.font = "15px sans-serif";
    ctx.fillStyle = colors.text;
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";

    var value = this.formatSkillValue(cell.type, cell.value);
    if (value) {
        // NOTE: HERE BE MAGIC NUMBERS!
        ctx.fillText(value, center.x, center.y + 24);
    }
};

MwoSkills.prototype.clickEventHexGrid = function(e) {
    var origin = this.hex.getOrigin();

    var localX = e.pageX - origin.x;
    var localY = e.pageY - origin.y;

    var tile = this.hex.getSelectedTile(localX, localY);
    if (tile) {
        var skill = this.getSkillFromCell(this.activeSection, tile.col, tile.row);
        if (skill) {
            if (!e.shiftKey) {
                this.toggleSkillState(this.activeSection, tile.col, tile.row);
                this.renderSection(this.activeSection);
            }
            else {
                var active = !this.isUnlocked(this.activeSection, tile.col, tile.row);
                var cells = this.skillData[this.activeSection].cells;
                for (var c of cells) {
                    if (c.type == skill.type) {
                        this.setSkillState(this.activeSection, c.col, c.row, active);
                    }
                }
                this.renderSection(this.activeSection);
            }
        }
    }
};

MwoSkills.prototype.mousemoveEventHexGrid = function(e) {
    if (this.isMobile) {
        return;
    }

    var origin = this.hex.getOrigin();

    var localX = e.pageX - origin.x;
    var localY = e.pageY - origin.y;

    var cursor = "default";

    var tile = this.hex.getSelectedTile(localX, localY);

    if (!tile || !this.lastOverTile ||
            tile.col != this.lastOverTile.col || tile.row != this.lastOverTile.row) {
        window.clearTimeout(this.showTip);
        this.showTip = undefined;
        var tip = document.querySelector('.tooltip');
        if (tip) {
            tip.remove();
        }
    }

    if (tile) {
        if ((e.buttons & 1) &&
                (this.lastOverTile.col != tile.col || this.lastOverTile.row != tile.row)) {
            this.clickEventHexGrid(e);
        }

        this.lastOverTile = tile;

        var skill = this.getSkillFromCell(this.activeSection, tile.col, tile.row);
        if (skill) {
            cursor = "pointer";

            var descr = "";
            if (skill.type in this.skillTypes) {
                descr = this.skillTypes[skill.type].descr;
            }

            if (!this.showTip) {
                var me = this;
                this.showTip = window.setTimeout(function() {
                    me.createToolTip(tile.col, tile.row, descr);
                }, 700);
            }
        }
        else if (this.editor) {
            cursor = "crosshair";

            // no-op for now. ideally, we'd highlight the "dummy cell"
        }
    }

    this.hex.canvas.style.cursor = cursor;
};

MwoSkills.prototype.createToolTip = function(col, row, text) {
    var canvasProps = this.hex.canvas.getBoundingClientRect();
    var origin = lineGrid.getOrigin();
    var center = this.hex.getCenterAtColRow(col, row);

    center.x += window.scrollX + canvasProps.left + origin.x - (this.hex.width - this.hex.side);
    center.y += window.scrollY + canvasProps.top  + origin.y + this.hex.height / 2;

    var oldtip = document.querySelector('.tooltip');
    if (oldtip) {
        oldtip.remove();
    }

    var tip = document.createElement('div');
    tip.className = 'tooltip';
    tip.appendChild(document.createTextNode(text));

    tip.style.top = (center.y + 6) + "px";
    tip.style.left = (center.x - 6) + "px";

    var first = document.body.firstChild;
    first.parentNode.insertBefore(tip, first);
};

MwoSkills.prototype.loadJSON = function(filename, callback) {
    var bound = callback.bind(this);

    var xr = new XMLHttpRequest();
    xr.overrideMimeType("application/json");
    xr.open("GET", filename, true);
    xr.timeout = 2000;
    xr.ontimeout = function() {
        bound(null);
    };
    xr.onload = function() {
        bound(JSON.parse(xr.responseText));
    };
    xr.send();
};

MwoSkills.prototype.getSkillColors = function(section, col, row) {
    var unlocked = this.isUnlocked(section, col, row);
    var skill = this.getSkillFromCell(section, col, row);

    function shadeColor(color, percent) {
        var f = parseInt(color.slice(1), 16);
        var t = percent < 0 ? 0 : 255;
        var p = percent < 0 ? percent * -1 : percent;
        var R = f >> 16;
        var G = f >> 8 & 0x00FF;
        var B = f & 0x0000FF;
        return "#" + (0x1000000 +
                      (Math.round((t-R)*p)+R)*0x10000 +
                      (Math.round((t-G)*p)+G)*0x100 +
                      (Math.round((t-B)*p)+B)).toString(16).slice(1);
    }

    // NOTE: HERE BE MAGIC NUMBERS!

    var stroke;
    var text;
    var fill = "#484848";

    var colormap = this.getStyleForCssId('#ColorMap_' + skill.type.split(' ').join(''), 'color');
    if (colormap) {
        function hex(x) {
            var hexDigits = new Array("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f");
            return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
        }
        function rgb2hex(rgb) {
            rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
            return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
        }
        fill = rgb2hex(colormap);
    }

    if (unlocked) {
        stroke = "#999999";
        text = "#e0e0e0";
    }
    else {
        stroke = "#373737";
        text = "#808080";
        fill = shadeColor(fill, -0.5);
    }

    return { stroke: stroke, text: text, fill: fill };
};

MwoSkills.prototype.isUnlocked = function(section, col, row) {
    var key = section + "," + col + "," + row;
    return (this.unlockedSkills.indexOf(key) >= 0);
};

MwoSkills.prototype.toggleSkillState = function(section, col, row) {
    var key = section + "," + col + "," + row;

    var index = this.unlockedSkills.indexOf(key);
    if (index >= 0) {
        this.unlockedSkills.splice(index, 1);
    }
    else {
        this.unlockedSkills.push(key);
    }

    this.saveSessionState();

    this.updateSkillCount();
    this.computeQuirkRollup();
};

MwoSkills.prototype.setSkillState = function(section, col, row, active) {
    var key = section + "," + col + "," + row;

    var index = this.unlockedSkills.indexOf(key);
    if (index >= 0 && !active) {
        this.unlockedSkills.splice(index, 1);
    }
    else if (index < 0 && active) {
        this.unlockedSkills.push(key);
    }

    this.saveSessionState();

    this.updateSkillCount();
    this.computeQuirkRollup();
}

MwoSkills.prototype.getSkillFromCell = function(section, col, row) {
    if (!this.skillData) {
        return;
    }
    if (!(section in this.skillData)) {
        return;
    }

    var cells = this.skillData[section].cells;
    for (var c of cells) {
        if (col == c.col && row == c.row) {
            return c;
        }
    }
};

MwoSkills.prototype.forEachUnlockedSkill = function(callback) {
    for (var key of this.unlockedSkills) {
        var fields = key.split(',');
        callback(fields[0], parseInt(fields[1]), parseInt(fields[2]));
    }
};

MwoSkills.prototype.setActiveSection = function(section) {
    var a;

    if (this.activeSection) {
        a = document.getElementById("nav_" + this.activeSection);
        if (a) {
            a.style.backgroundColor = "";
        }
    }

    // NOTE: HERE BE MAGIC NUMBERS!
    a = document.getElementById("nav_" + section);
    a.style.backgroundColor = "#977";

    this.activeSection = section;

    this.saveSessionState();

    this.renderSection(this.activeSection);
};

MwoSkills.prototype.clearElement = function(elem) {
    while (elem.hasChildNodes()) {
        elem.removeChild(elem.lastChild);
    }
};

MwoSkills.prototype.mirrorEvent = function() {
    var canvas = document.createElement('canvas');

    canvas.width = this.hex.canvas.width;
    canvas.height = this.hex.canvas.height;

    var context = canvas.getContext('2d');

    var bodyStyle = getComputedStyle(document.getElementsByTagName("body")[0]);
    context.fillStype = bodyStyle.backgroundColor;

    context.fillRect(0, 0, canvas.width, canvas.height);

    context.drawImage(this.lines.canvas, 0, 0);
    context.drawImage(this.hex.canvas, 0, 0);

    this.drawWatermark(canvas);

    this.hex.context.drawImage(canvas, 0, 0);
};

MwoSkills.prototype.renderAllSections = function() {
    var numSections = this.skillSections.length;
    if (numSections == 0) {
        return;
    }

    var wantedImageWidth = 3000;

    var maxRowWidth = 0;
    var maxRowHeight = 0;

    var sectionPlacement = {};
    var renderRow = 0;
    var renderRowWidth = 0;
    for (var s of this.skillSections) {
        var dim = this.computeSectionDimensions(s);

        if (renderRowWidth == 0 && dim.width > wantedImageWidth) {
            wantedImageWidth = dim.width;
        }
        if (dim.height > maxRowHeight) {
            maxRowHeight = dim.height;
        }
        if (maxRowWidth < renderRowWidth) {
            maxRowWidth = renderRowWidth;
        }

        if (dim.width + renderRowWidth > wantedImageWidth) {
            renderRowWidth = 0;
            renderRow++;
        }

        sectionPlacement[s] = { x: renderRowWidth,
                                width: dim.width,
                                row: renderRow };

        renderRowWidth += dim.width;
    }

    var canvas = document.createElement('canvas');

    canvas.width = maxRowWidth + 400;
    canvas.height = (renderRow + 1) * maxRowHeight;

    var context = canvas.getContext('2d');

    var bodyStyle = getComputedStyle(document.getElementsByTagName("body")[0]);

    context.fillStyle = bodyStyle.backgroundColor;
    context.fillRect(0, 0, canvas.width, canvas.height);

    this.drawWatermark(canvas);

    var grid = new HexagonGrid(canvas, this.hex.radius, this.hex.spacing);

    for (var s of this.skillSections) {
        var dim = sectionPlacement[s];

        var y = (dim.row * maxRowHeight);

        grid.setOrigin(dim.x + dim.width/2, y);
        this.renderSectionOnHexGrid(s, grid, grid);
    }

    context.font = "18px sans-serif";
    context.fillStyle = "#888";
    context.textAlign = "left";
    context.textBaseline = "bottom";

    var counter = document.getElementById("NodesUnlocked");
    context.fillText(counter.textContent + " unlocked nodes", maxRowWidth + 30, 250);

    var count = 0;
    var height = 300;
    for (var n of document.querySelectorAll("#QuirkRollup tr")) {
        var rowstyle = getComputedStyle(n);

        var left = n.childNodes[0];
        var leftstyle = getComputedStyle(left);

        var right = n.childNodes[1];
        var rightstyle = getComputedStyle(right);

        context.font = rowstyle.fontSize + " sans-serif";
        context.fillStyle = (count % 2 == 0) ? "#888" : "#bbb";
        context.textAlign = "left";
        context.textBaseline = "bottom";

        context.fillText(left.textContent, maxRowWidth + 30, height);

        context.textAlign = "right";
        context.fillText(right.textContent, maxRowWidth + parseInt(rowstyle.width), height);

        height += parseInt(rowstyle.height);
        count++;
    }

    return canvas;
};

MwoSkills.prototype.keyEvent = function(evt) {
    if (evt.key == '=') {
        if (this.editor) {
            this.disableEditor();
        }
        else {
            this.enableEditor();
        }
    }

    if (!this.editor) {
        return;
    }

    switch (evt.key) {
        case 'a':
            this.editorAddTile(this.lastOverTile);
            break;
        case 's':
            this.editorEditTile(this.lastOverTile);
            break;
        case 'd':
            this.editorDeleteTile(this.lastOverTile);
            break;
        case 'z':
            this.editorAddLink(this.lastOverTile, -1);
            break;
        case 'x':
            this.editorAddLink(this.lastOverTile, 0);
            break;
        case 'c':
            this.editorAddLink(this.lastOverTile, 1);
            break;
        case 'v':
            break;
        default:
            break;
    }
};

MwoSkills.prototype.downloadEvent = function() {
    var canvas = this.renderAllSections();

    var button = document.getElementById("downloader");
    button.href = canvas.toDataURL("image/png");
};

MwoSkills.prototype.selectallnodesEvent = function() {
    var cells = this.skillData[this.activeSection].cells;
    for (var c of cells) {
        var key = this.activeSection + "," + c.col + "," + c.row;

        var index = this.unlockedSkills.indexOf(key);
        if (index < 0) {
            this.unlockedSkills.push(key);
        }
    }

    this.saveSessionState();

    this.updateSkillCount();
    this.computeQuirkRollup();
    this.renderSection(this.activeSection);
};

MwoSkills.prototype.clearnodesEvent = function() {
    for (var i = this.unlockedSkills.length - 1; i >= 0; i--) {
        var fields = this.unlockedSkills[i].split(',');

        if (this.activeSection == fields[0]) {
            this.unlockedSkills.splice(i, 1);
        }
    }

    this.saveSessionState();

    this.updateSkillCount();
    this.computeQuirkRollup();
    this.renderSection(this.activeSection);
};

MwoSkills.prototype.drawWatermark = function(canvas) {
    var context = canvas.getContext('2d');

    // NOTE: HERE BE MAGIC NUMBERS!
    context.font = "10px sans-serif";
    context.fillStyle = "#888";
    context.textAlign = "right";
    context.textBaseline = "bottom";

    context.fillText("generated with kitlaan.gitlab.io/mwoskill " + this.version,
                     canvas.width - 10, canvas.height - 4);
    context.fillText("MWO version " + this.skillVersion + " (" + this.skillDate + ")",
                     canvas.width - 10, canvas.height - 18);
};

MwoSkills.prototype.updateSkillCount = function() {
    var count = 0;
    this.forEachUnlockedSkill(function(section, col, row) {
        count++;
    });

    var counter = document.getElementById("NodesUnlocked");
    this.clearElement(counter)
    counter.appendChild(document.createTextNode(count));
};

MwoSkills.prototype.formatSkillValue = function(name, value) {
    var output = "";

    if (typeof value == 'number') {
        // deal with javascript floating point accuracy
        var n = value < 0 ? -value : value;
        n = parseFloat(n).toPrecision(5).replace(/0+$/, '').replace(/\.$/, '');

        output += (value > 0) ? "+" : (value < 0) ? "\u2212" : "";
        output += n;
    }
    else {
        output += value;
    }

    if (output) {
        if (name in this.skillTypes) {
            var skilltype = this.skillTypes[name].type;
            switch (skilltype) {
                case "percent":
                    output += "%";
                    break;
                case "number":
                    break;
                case "none":
                    output = "";
                    break;
                default:
                    console.error("unknown value type '" + skilltype + "'");
                    break;
            }
        }
        else if (name === "") {
            output = "";
        }
        else {
            console.error("missing skill type '" + name + "'");
        }
    }

    return output;
};

MwoSkills.prototype.computeSectionValid = function(section) {
    // populate with all the cells in this section
    var rowcol = {};
    for (var c of this.skillData[section].cells) {
        if (!(c.row in rowcol)) {
            rowcol[c.row] = {};
        }
        rowcol[c.row][c.col] = c.links;
    }

    // starting with the root, remove unlocked cells that are direct
    // decendents of the root
    var unlockedCells = [];
    if (this.isUnlocked(section, 0, 0)) {
        unlockedCells.push({r:0, c:0});
    }

    while (unlockedCells.length > 0) {
        var coord = unlockedCells.pop();
        if (coord.r in rowcol && coord.c in rowcol[coord.r]) {
            for (var l of rowcol[coord.r][coord.c]) {
                if (this.isUnlocked(section, l.col, l.row)) {
                    unlockedCells.push({r:l.row, c:l.col});
                }
            }
            delete rowcol[coord.r][coord.c];
        }
    }

    // make a list of invalid cells
    var invalidcells = [];
    for (var r in rowcol) {
        for (var c in rowcol[r]) {
            if (this.isUnlocked(section, c, r)) {
                invalidcells.push(c + "," + r);
            }
        }
    }
    return invalidcells;
};

MwoSkills.prototype.computeSectionDimensions = function(section) {
    var maxY = 0;
    var minX = 0;
    var maxX = 0;

    for (var c of this.skillData[section].cells) {
        var offset = this.hex.getOffsetAtColRow(c.col, c.row);
        if (offset.x < minX) {
            minX = offset.x;
        }
        if (offset.x + this.hex.width + this.hex.spaceY > maxX) {
            maxX = offset.x + this.hex.width + this.hex.spaceY;
        }
        if (offset.y + this.hex.height + this.hex.spaceY > maxY) {
            maxY = offset.y + this.hex.height + this.hex.spaceY;
        }
    }

    if (Math.abs(minX) > maxX) {
        maxX = Math.abs(minX);
    }

    return { width: maxX * 2, height: maxY };
};

MwoSkills.prototype.computeQuirkRollup = function() {
    var quirks = {};
    var names = [];

    var me = this;
    this.forEachUnlockedSkill(function(section, col, row) {
        var cell = me.getSkillFromCell(section, col, row);
        if (!(cell.type in quirks)) {
            quirks[cell.type] = 0;
            names.push(cell.type);
        }
        quirks[cell.type] += cell.value;
    });

    names.sort();

    var output = document.getElementById("QuirkRollup");
    this.clearElement(output);
    for (var n of names) {
        var row = document.createElement('tr');

        var col1 = document.createElement('td');
        col1.appendChild(document.createTextNode(n));
        row.appendChild(col1);

        var col2 = document.createElement('td');
        col2.appendChild(document.createTextNode(this.formatSkillValue(n, quirks[n])));
        col2.style.textAlign = 'right';
        row.appendChild(col2);

        output.appendChild(row);
    }
};

MwoSkills.prototype.getStyleForCssId = function(selector, style) {
    for (var i = 0, l = document.styleSheets.length; i < l; i++) {
        var sheet = document.styleSheets[i];
        if (!sheet.cssRules) {
            continue;
        }
        for (var j = 0, k = sheet.cssRules.length; j < k; j++) {
            var rule = sheet.cssRules[j];
            if (rule.selectorText && rule.selectorText.split(',').indexOf(selector) !== -1) {
                return rule.style[style];
            }
        }
    }
    return null;
};

MwoSkills.prototype.makeModal = function(dialogid, buttonid) {
    var modal = document.getElementById(dialogid);
    var button = document.getElementById(buttonid);
    var closer = modal.getElementsByClassName("close")[0];

    button.style.cursor = "pointer";

    button.onclick = function() {
        modal.style.display = "block";
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    }

    closer.onclick = function() {
        modal.style.display = "none";
        window.onclick = undefined;
    }
};

MwoSkills.prototype.saveSessionState = function() {
    sessionStorage.setItem('section', this.activeSection);
    sessionStorage.setItem('selections', JSON.stringify(this.unlockedSkills));
};

MwoSkills.prototype.loadSessionState = function() {
    if ('section' in sessionStorage) {
        this.activeSection = sessionStorage.section;
    }
    if ('selections' in sessionStorage) {
        this.unlockedSkills = JSON.parse(sessionStorage.selections);
    }
};

MwoSkills.prototype.enableEditor = function() {
    this.editor = true;
    var me = this;

    this.oldSkills = {};

    var skillModal = document.querySelector("#SelectVersionModal div.modalContent");

    var textarea = document.createElement("textarea");
    textarea.id = "SelectVersionEditText";
    skillModal.append(textarea);

    var buttonload = document.createElement("input");
    buttonload.id = "SelectVersionEditLoad";
    buttonload.value = "Load";
    buttonload.type = "button";
    skillModal.append(buttonload);
    buttonload.onclick = function() {
        var obj = JSON.parse(textarea.value);
        me.skillData[me.activeSection] = obj;

        me.updateSkillCount();
        me.computeQuirkRollup();

        me.renderSection(me.activeSection);
    }

    var buttongen = document.createElement("input");
    buttongen.id = "SelectVersionEditGen";
    buttongen.value = "Generate";
    buttongen.type = "button";
    skillModal.append(buttongen);
    buttongen.onclick = function() {
        var json = JSON.stringify(me.skillData[me.activeSection]);
        textarea.value = json;
    }

    this.debug = true;
    this.renderSection(this.activeSection);
};

MwoSkills.prototype.disableEditor = function() {
    this.editor = false;

    var textarea = document.getElementById("SelectVersionEditText");
    if (textarea) {
        textarea.parentElement.removeChild(textarea);
    }

    var buttonload = document.getElementById("SelectVersionEditLoad");
    if (buttonload) {
        buttonload.parentElement.removeChild(buttonload);
    }

    var buttongen = document.getElementById("SelectVersionEditGen");
    if (buttongen) {
        buttongen.parentElement.removeChild(buttongen);
    }

    this.debug = false;
    this.renderSection(this.activeSection);
};

MwoSkills.prototype.editorAddTile = function(tile) {
    if (!tile || !this.editor) {
        return;
    }

    var skill = this.getSkillFromCell(this.activeSection, tile.col, tile.row);
    if (!skill) {
        var key = this.activeSection + "," + tile.col + "," + tile.row;

        if (key in this.oldSkills) {
            skill = this.oldSkills[key];
            delete this.oldSkills[key];
        }
        else {
            skill = {
                row: tile.row,
                col: tile.col,
                type: "",
                level: 0,
                value: 0,
                links: [],
            };
        }

        this.skillData[this.activeSection].cells.push(skill);

        this.renderSection(this.activeSection);
    }
};

MwoSkills.prototype.editorDeleteTile = function(tile) {
    if (!tile || !this.editor) {
        return;
    }

    var skill = this.getSkillFromCell(this.activeSection, tile.col, tile.row);
    if (skill) {
        var key = this.activeSection + "," + tile.col + "," + tile.row;
        this.oldSkills[key] = skill;

        var index = this.skillData[this.activeSection].cells.indexOf(skill);
        if (index >= 0) {
            this.skillData[this.activeSection].cells.splice(index, 1);
        }

        this.renderSection(this.activeSection);
    }
};

MwoSkills.prototype.editorEditTile = function(tile) {
    if (!tile || !this.editor) {
        return;
    }

    var skill = this.getSkillFromCell(this.activeSection, tile.col, tile.row);
    if (skill) {
        var field = window.prompt("Skill?", skill.type);
        var level = window.prompt("Level?", skill.level);
        var value = window.prompt("Value?", skill.value);

        skill.type = field.trim();
        skill.level = parseInt(level);
        skill.value = parseFloat(value);

        this.renderSection(this.activeSection);
    }
};

MwoSkills.prototype.editorAddLink = function(tile, direction) {
    if (!tile || !this.editor) {
        return;
    }

    var skill = this.getSkillFromCell(this.activeSection, tile.col, tile.row);
    if (skill) {
        var linkrow = tile.row;
        if (direction == 0) {
            linkrow += 1;
        }
        else {
            linkrow += (tile.col % 2 == 0) ? 1 : 0;
        }

        var linkcol = tile.col + direction;

        var index = 0;
        while (index < skill.links.length) {
            var l = skill.links[index];
            if (l.col == linkcol && l.row == linkrow) {
                break;
            }
            index++;
        }

        if (index == skill.links.length) {
            skill.links.push({col: linkcol, row: linkrow});
        }
        else {
            skill.links.splice(index, 1);
        }

        this.renderSection(this.activeSection);
    }
};
